#include <stdio.h>
#include <string.h>
#include <unistd.h> // 引入unistd.h以使用read, write, close等系统调用
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#define BUFFER_SIZE 1024


int main(int argc, char* argv[])
{   
    //if(argc < 2) {
        //printf("Usage: ./myproc arg1 [arg2 ...]\n");
        //return -1;
    //}
    //int fd = open("test.txt", O_WRONLY | O_CREAT | O_TRUNC,0666); //输出重定向
    //int fd = open("test.txt", O_WRONLY | O_CREAT | O_APPEND,0666); //追加输出
    int fd = open("test.txt", O_RDONLY); //只读
    if (fd == -1) {
        perror("Error opening file");
        return 1;
    }
    dup2(fd, 0); //将标准输入重定向到文件
    //fprintf(stdout,"%s\n", argv[1]);
    char buffer[BUFFER_SIZE];
    memset(buffer, '\0', BUFFER_SIZE);
    while(fgets(buffer, BUFFER_SIZE, stdin)!=NULL)
    {
        printf("buffer: %s\n", buffer);
    }
    close(fd);
}

//int main()
//{
//    int fd = open("test.txt", O_RDWR | O_CREAT, 0666); // 使用O_RDWR以读写模式打开文件
//    if (fd == -1) {
//        perror("Error opening file");
//        return 1;
//    }
//        int fd1 = open("log1.txt", O_WRONLY|O_CREAT|O_APPEND, 0666); //rw-rw-rw-
//        printf("open success, fd: %d\n", fd1);
//        int fd2 = open("log2.txt", O_WRONLY|O_CREAT|O_APPEND, 0666); //rw-rw-rw-
//        printf("open success, fd: %d\n", fd2);
//        int fd3 = open("log3.txt", O_WRONLY|O_CREAT|O_APPEND, 0666); //rw-rw-rw-
//        printf("open success, fd: %d\n", fd3);
//        int fd4 = open("log4.txt", O_WRONLY|O_CREAT|O_APPEND, 0666); //rw-rw-rw-
//        printf("open success, fd: %d\n", fd4);
//    // 写入数据到文件
//const char* write_str = "Hello, world!";
//ssize_t bytes_written = write(fd, write_str, strlen(write_str));
//if (bytes_written == -1) {
//    perror("Error writing to file");
//    close(fd);
//    return 1;
//}
//
//// 将文件指针重置到文件开头
//lseek(fd, SEEK_SET, 0);
//
//// 读取数据从文件
//char buffer[BUFFER_SIZE];
//memset(buffer, '\0', BUFFER_SIZE);// 初始化buffer
//ssize_t bytes_read = read(fd, buffer,BUFFER_SIZE); 
//if (bytes_read == -1) {
//    perror("Error reading from file");
//    close(fd);
//    return 1;
//}
//
//
//// 打印从文件读取的内容
//printf("Read from file: %s\n", buffer);

// 关闭文件
//close(fd);
//close(fd1);
//close(fd2);
//close(fd3);
//close(fd4);
//
//    return 0;
//}

////////////////////////////////////////////////////

//int main()
//{
//    printf("Hello world!\n");
//    const char* str = "Hello world!";
//    fputs(str, stdout);
//    fprintf(stdout, "Hello world!\n");
//    return 0;
//}


///////////////////////////////////////////////////
//int main()
//{   FILE* fd = fopen("my.txt", "r");
//    if(fd == NULL)
//    {   printf("Error opening file\n");
//        return 1;
//    }
//    const char* str = "Hello world!";
//    char buffer[BUFFER_SIZE];
//    memset(buffer, '\0', BUFFER_SIZE);
//    //fwrite(str, 1, strlen(str), fd);
//    while(1)
//    {
//        size_t f = fread(buffer, 1, strlen(str), fd);
//        if(f>0)
//        {   buffer[f] = 0;
//            printf("%s", buffer);
//        }
//        if(feof(fd))
//            break;
//       
//    }
//    fclose(fd);
//    return 0;
//}
