#include <iostream>
#include <cstring>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>
using namespace std;


void handler(int signo)
{
    pid_t rid;
    while ((rid = waitpid(-1, nullptr, WNOHANG)) > 0)
    {
        cout << "I am proccess: " << getpid() << " catch a signo: " << signo << "child process quit: " << rid << endl;
    }
    
}


int main()
{
    signal(17,SIG_IGN);
    for(int i = 0 ; i<10 ; i++)
    {
        pid_t pid = fork();
        if(pid == 0)
        {
            while(true)
            {
                cout << "I am a child process: " << getpid() << " ppid: " << getppid() << endl;
                sleep(1);
                break;
            }
            cout << " child quit !!! " << endl;
            exit(0);
        }
        sleep(1);

    }
    while (true)
    {
        cout << "I am father process: " << getpid() << endl;
        sleep(1);
    }

    return 0;
}






// volatile int n = 0;
// void handler(int signo)
// {
//     cout << " catch a  signal ,signal number: " << signo << endl;
//     n = 1;
// }

// int main()
// {
//     signal(2, handler);
//     while (!n);
//     cout << "process quit normal" << endl;
//     return 0;
// }
// void PrintPending()
// {
//     sigset_t set;
//     sigpending(&set);
//     for (int signo = 31; signo >= 1; signo--)
//     {
//         if (sigismember(&set, signo))
//         {
//             cout << "1";
//         }
//         else
//             cout << "0";
//     }
//     cout << endl;
// }

// void handler(int signo)
// {
//     cout << " catch a  signal ,signal number: " << signo << endl;
//     while (true)
//     {
//         PrintPending();
//         sleep(1);
//     }
// }

// int main()
// {
//     // 1.使用sigaction函数
//     struct sigaction act, oact;
//     // 1.1初始化 act oact。
//     memset(&act, 0, sizeof(act));
//     memset(&oact, 0, sizeof(oact));
//     // 2.1 初始化 并设置信号屏蔽集
//     sigemptyset(&act.sa_mask);
//     sigaddset(&act.sa_mask, 1);
//     sigaddset(&act.sa_mask, 3);
//     sigaddset(&act.sa_mask, 4);
//     // 3.自定义handler函数
//     act.sa_handler = handler;
//     //3.1 给2号信号注册自定义动作。
//     sigaction(2, &act, &oact);
//     while (true)
//     {
//         cout << "I am a process: " << getpid() << endl;
//         sleep(1);
//     }
//     // 4. 发送信号
//     return 0;
// }