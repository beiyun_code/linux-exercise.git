#pragma once
#include <iostream>
#include <cstring>
#include <string>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <functional>
#include <unordered_map>
#include "log.hpp"

typedef std::function<std::string(const std::string &, const std::string &, uint16_t )> func_t;
Log lg;
enum
{
    SOCKET_ERR = 1,
    BIND_ERR
};
std::string defaultip = "0.0.0.0";
uint16_t defaultport = 8080;
const int size = 1024;
class Udpserver
{
public:
    Udpserver(const uint16_t &port = defaultport, const std::string &ip = defaultip)
        : _socketfd(0), _port(port), _ip(ip)
    {
    } // 构造函数

    void Init()
    { // 1.创建套接字
        _socketfd = socket(AF_INET, SOCK_DGRAM, 0);
        if (_socketfd < 0)
        {
            lg(Fatal, "创建套接字失败: %d", _socketfd);
            exit(SOCKET_ERR);
        }
        lg(Info, "创建套接字成功：%d", _socketfd);
        // 2. 绑定套接字
        struct sockaddr_in local;
        bzero(&local, sizeof(local)); // 清空local
        local.sin_family = AF_INET;
        local.sin_port = htons(_port);                  //??
        local.sin_addr.s_addr = inet_addr(_ip.c_str()); //??
        if (bind(_socketfd, (const struct sockaddr *)&local, sizeof(local)) < 0)
        {
            lg(Fatal, "绑定套接字失败,错误码：%d, 错误信息:%s", errno, strerror(errno));
            exit(BIND_ERR);
        }
        lg(Info, "绑定套接字成功：%d ,%s", errno, strerror(errno));
    }
    void CheckUser(const struct sockaddr_in &client, const std::string &clientip, uint16_t clientport)
    {
        auto iter = _online_user.find(clientip);
        if (iter == _online_user.end()) // 迭代器走到end，说明是新用户。插入哈希表
        {
            _online_user.insert({clientip, client});
            std::cout << "[" << clientip << ":" << clientport << "] add to online user." << std::endl;
        }
    }
    // 想想QQ群或者微信群，不论谁发消息，在群里的人都能收到一条xx谁发的消息。下面我们直接写一个播送函数
    void BroadCast(const std::string &info, const std::string &clientip, uint16_t clientport)
    {
        for(auto & user:_online_user)
        {
            socklen_t len = sizeof(user.second);
            sendto(_socketfd,info.c_str(),info.size(),0,(struct sockaddr*)(&user.second),len);
        }
    }

    void Run(func_t func)
    {
        char buf[size];
        while (true)
        {
            struct sockaddr_in client;
            socklen_t len = sizeof(client);
            ssize_t n = recvfrom(_socketfd, buf, sizeof(buf) - 1, 0, (struct sockaddr *)&client, &len);
            if (n < 0)
            {
                lg(Warning, "接收消息失败,错误码:%d,错误信息:%s", errno, strerror(errno));
                continue;
            }

            buf[n] = 0; // 这里我们直接当字符串使用。
            // 这里我们简单处理一下数据，客户发什么，我们回响什么。
            uint16_t clientport = ntohs(client.sin_port);
            std::string clientip = inet_ntoa(client.sin_addr);
            CheckUser(client,clientip,clientport);
            std::string info = buf;
            std::string ehco_string = func(info,clientip,clientport);
            // sendto(_socketfd, ehco_string.c_str(), ehco_string.size(), 0, (const struct sockaddr *)&client, len);
            BroadCast(ehco_string,clientip,clientport);
        }
    }
    ~Udpserver() // 析构函数
    {
        if (_socketfd > 0)
            close(_socketfd);
    }

private:
    int _socketfd;
    std::string _ip;
    uint16_t _port;
    std::unordered_map<std::string, struct sockaddr_in> _online_user;
};