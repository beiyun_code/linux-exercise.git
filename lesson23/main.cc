#include "Udpsever.hpp"
#include "log.hpp"
#include <memory>
#include <cstdio>
void Usage(std::string proc)
{
    std::cout << "\n\rUsage: " << proc << " port[1024+]\n"
              << std::endl;
}

std::string ExcuteCommand(const std::string &cmd)
{
    FILE *fp = popen(cmd.c_str(), "r");
    if (nullptr == fp)
    {
        perror("popen");
        return "error";
    }
    std::string result;
    char buffer[4096];
    while (true)
    {
        char *str = fgets(buffer, sizeof(buffer), fp);
        if (str == nullptr)
            break;
        result += str;
    }
    return result;
}
std::string Handler(const std::string &info, const std::string &clientip, uint16_t clientport)
{
    std::string message = "[";
    message += clientip;
    message += ":";
    message += std::to_string(clientport);
    message += "]# ";
    message += info;
    return message;
}
int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        Usage(argv[0]);
        exit(0);
    }

    uint16_t port = std::stoi(argv[1]);

    std::unique_ptr<Udpserver> svr(new Udpserver(port));
    svr->Init();
    svr->Run(Handler);
    return 0;
}