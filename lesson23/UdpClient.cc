#include <iostream>
#include <string>
#include <cstring>
#include <unistd.h>
#include <cstdlib>
#include <pthread.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

struct ThreadData
{
    struct sockaddr_in server;
    int sockfd;
};
void Usage(std::string proc)
{
    std::cout << "\n\rUsage: " << proc << " serverip serverport\n"
              << std::endl;
}
void *recv_message(void *args)
{
    ThreadData *td = static_cast<ThreadData *>(args);
    char buffer[1024];
    while (true)
    {

        struct sockaddr_in temp;
        socklen_t len = sizeof(temp);
        ssize_t n = recvfrom(td->sockfd, buffer, 1023, 0, (struct sockaddr *)&temp, &len);
        if (n > 0)
        {
            buffer[n] = 0;
            std::cout << buffer << std::endl;
        }
    }
}
void *send_message(void *args)
{
    ThreadData *td = static_cast<ThreadData *>(args);
    std::string message;
    char buffer[1024];
    socklen_t len = sizeof(td->server);
    while (true)
    {
        std::cout << "请输入@:";
        std::getline(std::cin, message);
        sendto(td->sockfd, message.c_str(), message.size(), 0, (struct sockaddr *)(&td->server), len);
    }
}

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        Usage(argv[0]);
        exit(1);
    }
    struct ThreadData td;

    std::string serverip = argv[1];
    uint16_t serverport = std::stoi(argv[2]);
    bzero(&(td.server), sizeof(td.server));
    td.server.sin_family = AF_INET;
    td.server.sin_port = htons(serverport);                  //?
    td.server.sin_addr.s_addr = inet_addr(serverip.c_str()); //?
    // 1.创建套接字
    td.sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (td.sockfd < 0)
    {
        std::cout << "创建套接字失败" << std::endl;
        return 1;
    }
    // std::string message;

    // while (true)
    // {
    //     std::cout << "请输入@:";
    //     std::getline(std::cin, message);
    //     sendto(sockfd, message.c_str(), message.size(), 0, (struct sockaddr *)&server, len);
    //     struct sockaddr_in temp;
    //     socklen_t len = sizeof(temp);
    //     ssize_t n = recvfrom(sockfd, buffer, 1023, 0, (struct sockaddr *)&temp, &len);
    //     if (n > 0)
    //     {
    //         buffer[n] = 0;
    //         std::cout << buffer << std::endl;
    //     }
    // }
    pthread_t recv, send;

    pthread_create(&recv, nullptr, recv_message, &td);
    pthread_create(&send, nullptr, send_message, &td);

    pthread_join(recv, nullptr);
    pthread_join(send, nullptr);
    close(td.sockfd);
    return 0;
}