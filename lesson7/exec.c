#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/types.h>



#define NUM 1024
#define SEP " "    //定义分割标识符
#define SIZE 64
//保存完整的命令行字符串
char cmd_line[NUM]; //用来保存用户输入的字符串
//保存被打散的字符串
char* g_argv[SIZE];
int main()
{
    while(1)
    {
        //1. 打印出提示信息 [root@localhost myshell]# 
        printf("[root@localhost myshell]# ");
        fflush(stdout);
        //初始化缓冲区数组
        memset(cmd_line, '\0', sizeof cmd_line);
        //2. 获取用户的键盘输入[输入的是各种指令和选项: "ls -a -l -i"]
        //如果用户输入错误重新开始
        if(fgets(cmd_line, sizeof cmd_line, stdin) == NULL)
        {
            continue;
        }
        cmd_line[strlen(cmd_line)-1] = '\0';
        //3.命令行字符串解析
        g_argv[0] = strtok(cmd_line,SEP); //第一次调用，要传入原始字符串
        int index = 1;
        while(g_argv[index++] = strtok(NULL,SEP));
        //5.内置命令, 让父进程（shell）自己执行的命令，我们叫做内置命令，内建命令
        //内建命令本质其实就是shell中的一个函数调用
        if(strcmp(g_argv[0], "cd") == 0) 
        {
            if(g_argv[1] != NULL) chdir(g_argv[1]);//chdir 可以帮我们自动切换到指定路径
            continue;
        }
        //5.创建子进程
        pid_t id = fork();
        if(id <0) 
        {    
            perror("fork");
            exit(1);
        }
        if(id == 0)
        {
             printf("下面功能让子进程进行的\n");
             execvp(g_argv[0], g_argv); // ls -a -l -i
             exit(1);
        }
        //father
        int status = 0;
        pid_t ret = waitpid(id, &status, 0);
        if(ret > 0)printf("exit code: %d\n", WEXITSTATUS(status));
    }
   
}
