#include <iostream>
#include <sys/types.h>
#include <sys/wait.h>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <string>
#include <unistd.h>
#define N 2

void Write(int fd)
{
    std::string str = "hello, I am child process";
    pid_t pid = getpid();
    int number = 0;
    char buf[1024];
    while (1)
    {
        sleep(1);
        buf[0] = '\0';
        snprintf(buf, sizeof(buf), "%s-%d- %d\n", str.c_str(), number++, pid);
        write(fd, buf, strlen(buf));
        // std::cout <<number << std::endl;
        // if(number > 5)
        // break;
    }
}

void Read(int fd)
{
    char buf[1024];
    int cnt = 0;
    while (1)
    {

        memset(buf, 0, sizeof(buf));
        size_t n = read(fd, buf, sizeof(buf));
        if (n > 0)
        {
            std::cout << "father get a message:[" << getpid() << "]#" << buf << std::endl;
        }
        else if (n == 0)
        {
            printf("father read file done\n");
            break;
        }
        else
        {
            std::cout << "father read error" << std::endl;
            break;
        }
        cnt++;
        if (cnt > 5)
            break;
    }
}

int main()
{
    int pipefd[N];
    int n = pipe(pipefd);
    if (n < 0)
    {
        std::cout << "pipe error" << std::endl;
        return 1;
    }
    pid_t pid = fork();
    if (pid < 0)
    {
        std::cout << "fork error" << std::endl;
    }
    else if (pid == 0)
    {
        // child process
        close(pipefd[0]);
        Write(pipefd[1]);
        close(pipefd[1]);
        exit(0);
    }
    else
    {
        // parent process
        Read(pipefd[0]);
        close(pipefd[0]);
        // wait child process
        std::cout << "father close read fd: " << pipefd[0] << std::endl;
        sleep(5); // wait child process exit
        pid_t status = 0;
        pid_t child_pid = waitpid(pid, &status, 0);
        if (child_pid < 0)
        {
            std::cout << "waitpid error" << std::endl;
            return 2;
        }
        std::cout << "wait child success: " << child_pid << " exit code: " << ((status >> 8) & 0xFF)
                  << " exit signal: " << (status & 0x7F) << std::endl;
    }

    sleep(3); // wait father process exit
    return 0;
}