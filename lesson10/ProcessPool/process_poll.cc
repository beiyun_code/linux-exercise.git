#include "task.hpp"
#include <string>
#include <unistd.h>
#include <cstdlib>
#include <ctime>
#include <sys/types.h>
#include <sys/wait.h>

#define ProcessNum 5 // 进程个数
std::vector<task_t> tasks;
// 先描述
class channel
{
public:
    channel(int cmdfd, int slaverid, const std::string &processname)
        : _cmdfd(cmdfd), _slaverid(slaverid), _processname(processname)
    {
    }

public:
    int _cmdfd;               // 发送任务的文件描述符
    pid_t _slaverid;          // 子进程的PID
    std::string _processname; // 子进程的名字
};
void slaver()
{
    int cmdcode = 0;
    while (true)
    {
        int n = read(0, &cmdcode, sizeof(int));
        if (n == sizeof(int))
        {
            std::cout << "slaver say@ get a cmdcode: " << getpid() << " : cmdcode:" << cmdcode << std::endl;
            if (cmdcode >= 0 && cmdcode < tasks.size())
                tasks[cmdcode]();
        }
        if (n == 0)
            break;
    }
}
void InitProcessPool(std::vector<channel> *channels)
{
    std::vector<int> oldfds;
    for (int i = 0; i < ProcessNum; i++)
    {
        // 创建管道
        int pipefd[2];
        int n = pipe(pipefd);
        if (n < 0)
        {
            perror("pipe");
            exit(1);
        }
        // 创建子进程
        pid_t id = fork();
        if (id < 0)
        {
            perror("fork");
            exit(2);
        }
        else if (id == 0)
        { // 子进程
            for (auto fd : oldfds) //关闭之前继承下来的写端
            {
                close(fd);
            }
            close(pipefd[1]);   // 子进程读，关闭写端。
            dup2(pipefd[0], 0); // 管道的读端替换成标准输入0
            close(pipefd[0]);
            slaver();
            exit(0);
        }
        else
        {
            // 父进程
            close(pipefd[0]); // 父进程写，关闭读端。
            // 添加channle字段
            std::string name = "process-" + std::to_string(i);
            channels->push_back(channel(pipefd[1], id, name)); // 利用零时对象初始化
            oldfds.push_back(pipefd[1]);                       // 子进程会继承父进程的写端 方便我们在fork之后关闭写端
        }
    }
}
void Debug(const std::vector<channel> &channels)
{
    for (auto &it : channels)
    {
        std::cout << it._cmdfd << ' ' << it._slaverid << ' ' << it._processname << std::endl;
    }
}

void ctrlSlaver(const std::vector<channel> &channels)
{
    while (true)
    {
        std::cout << "Please Enter@ ";
        // 1. 选择任务
        int cmdcode = rand() % tasks.size();
        // 2. 选择进程
        int processpos = rand() % channels.size();

        std::cout << "father say: "
                  << " cmdcode: " << cmdcode << " already sendto " << channels[processpos]._slaverid << " process name: "
                  << channels[processpos]._processname << std::endl;
        // 3. 发送任务
        write(channels[processpos]._cmdfd, &cmdcode, sizeof(cmdcode));
        // sleep(1);
    }
}
void QuitProcess(const std::vector<channel> &channels)
{
    for (const auto &c : channels)
    {
        close(c._cmdfd);
        waitpid(c._slaverid, nullptr, 0);
    }
}
int main()
{
    LoadTask(&tasks);
    srand(time(nullptr) ^ getpid() ^ 1023); // 种一个随机数种子
    // 再组织
    std::vector<channel> channels;
    // 初始化
    InitProcessPool(&channels);
    // 控制子进程
    ctrlSlaver(channels);
    // Debug(channels);
    // 清理收尾
    QuitProcess(channels);
    return 0;
}
