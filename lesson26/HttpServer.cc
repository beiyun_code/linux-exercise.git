#include "HttpServer.hpp"
#include <iostream>
#include <memory>


int main()
{
    std::unique_ptr<HttpServer> svr(new HttpServer());
    svr->Init();
    svr->Start();
    return 0;
}