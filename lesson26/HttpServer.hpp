#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <unordered_map>
#include <fstream>
#include <thread>
#include "Socket.hpp"

const std::string wwwroot = "./wwwroot";
const std::string sep = "\r\n";
const std::string homepage = "index.html";
const std::string contentype = "./wwwroot/content_type.txt";
const std::string sep1 = ":";
static const uint16_t defaultport = 8080;

class HttpServer;
class ThreadData
{
public:
    ThreadData(int sockfd, HttpServer *tpsvr)
        : _sockfd(sockfd), _tpsvr(tpsvr) {}

public:
    int _sockfd;
    HttpServer *_tpsvr; // 回调指针
};

class HttpRequest
{
public:
    // 进行反序列化
    void Deserialization(std::string req)
    {
        size_t pos = 0;
        while ((pos = req.find(sep)) != std::string::npos)
        {
            size_t next_pos = pos + sep.size();
            if (pos > 0)
            { // 确保不是空字符串
                req_header.push_back(req.substr(0, pos));
            }
            req.erase(0, next_pos);
        }
        // 循环退出后，剩下的就是报文的正文部分
        text = req;
    }
    // 解析请求行
    void Parse()
    {
        std::stringstream ss(req_header[0]);
        ss >> method >> url >> http_version;
        file_path = wwwroot;
        if (url == "/" || url == "/index.html") // 访问根目录 就只返回网站首页
        {
            file_path += "/";
            file_path += homepage;
        }
        else
            file_path += url; // 访问其他路径

        auto pos = file_path.rfind("."); // 找路径文件后缀格式
        if (pos == std::string::npos)
        {
            suffix = ".htlm";
        }
        else
        {
            suffix = file_path.substr(pos); // 找到了，文件后缀格式放在容器中
        }
    }
    void DebugPrint()
    {
        for (auto &line : req_header)
        {
            std::cout << "--------------------------------" << std::endl;
            std::cout << line << "\n\n";
        }

        std::cout << "method: " << method << std::endl;
        std::cout << "url: " << url << std::endl;
        std::cout << "http_version: " << http_version << std::endl;
        std::cout << "file_path: " << file_path << std::endl;
        std::cout << text << std::endl;
    }

public:
    std::vector<std::string> req_header;
    std::string text;
    // 解析之后的结果
    std::string method;
    std::string url;
    std::string http_version;
    std::string file_path;
    std::string suffix; // 资源后缀格式
};

class HttpServer
{
public:
    HttpServer(uint16_t port = defaultport)
        : _port(port) {}
    void Init()
    {
        _listensock.Socket();
        _listensock.Bind(_port);
        _listensock.Listen();

        std::ifstream in(contentype);
        if (!in.is_open())
        {
            lg(Fatal, "isfstream open error %s", contentype.c_str());
            exit(1);
        }
        std::string line;
        while (std::getline(in, line))
        {
            std::string part1, part2;
            Split(line, &part1, &part2);
            content_type.insert({part1, part2});
        }
        in.close();
    }

    // 将content_type.txt 分割成 哈希键值对 后序插入
    bool Split(const std::string &s, std::string *part1, std::string *part2)
    {
        auto pos = s.find(sep1);
        if (pos == std::string::npos)
            return false;
        *part1 = s.substr(0, pos);
        *part2 = s.substr(pos + 1);
        return true;
    }

    // 根据解析的路径确定打开那个文件
    static std::string ReadHtmlContent(const std::string &htmlpath)
    {
        std::ifstream in(htmlpath, std::ios::binary); // 按二进制打开
        if (!in.is_open())
        {
            return "";
        }
        std::string content;
        in.seekg(0, std::ios::end); // 找到文件的最后位置
        auto len = in.tellg();      // 算出文件的长度
        in.seekg(0, std::ios::beg); // 文件最后位置复位
        content.resize(len);
        in.read((char *)content.c_str(), content.size());
        // std::string line;
        // while (std::getline(in, line))
        // {
        //     content += line;
        // }
        in.close();
        return content;
    }

    static void ThreaRun(ThreadData *td)
    {
        // 先简单处理
        int sockfd = td->_sockfd;
        char buffer[10240];
        int n = recv(sockfd, buffer, sizeof(buffer) - 1, 0);
        if (n > 0)
        {
            buffer[n] = 0;
            std::cout << buffer << std::endl;
            HttpRequest req;
            req.Deserialization(buffer);
            req.Parse();
            // req.DebugPrint();
            //  返回响应
            std::string text = ReadHtmlContent(req.file_path); // 响应的内容
            bool ok = true;
            if (text.empty())
            {
                ok = false;
                std::string err_html = wwwroot;
                err_html += "/err.html";
                text = ReadHtmlContent(err_html);
            }
            std::string response_line;
            if (ok)
                response_line = "HTTP/1.0 200 OK\r\n"; // 响应行
            else
                response_line = "HTTP/1.0 404 Not Found\r\n";

            // response_line = "HTTP/1.0 302 Found\r\n"; //重定向
            std::string response_header = "Content-Length:"; // 响应报文
            response_header += std::to_string(text.size());  // 内容的长度
            response_header += "\r\n";
            response_header += "Content-Type:";
            response_header += td->_tpsvr->SuffixToDesc(req.suffix);
            response_header += "\r\n";
            // response_header += "Location: https://www.baidu.com\r\n";//重定向到百度
            response_header += "Set-Cookie:name=haha";
            response_header += "\r\n";
            response_header += "Set-Cookie:passwd=123456";
            response_header += "\r\n";
            std::string bank_line = "\r\n"; // 空行

            std::string response = response_line;
            response += response_header;
            response += bank_line;
            response += text;

            // 发送报文
            send(td->_sockfd, response.c_str(), response.size(), 0);
        }
        delete td;
    }

    std::string SuffixToDesc(const std::string &suffix)
    {
        auto iter = content_type.find(suffix);
        if (iter == content_type.end())
            return content_type[".html"];
        else
            return content_type[suffix];
    }
    void Start()
    {
        for (;;)
        {
            std::string clientip;
            uint16_t clientport;
            int sockfd = _listensock.Accept(&clientip, &clientport);
            if (sockfd < 0)
                continue;
            lg(Info, "get a new link, clientip: %s, clientport: %d", clientip.c_str(), clientport);
            // 创建线程处理请求
            ThreadData *td = new ThreadData(sockfd, this);
            std::thread t(ThreaRun, td);
            t.detach();
        }
    }

private:
    Sock _listensock;
    uint16_t _port;
    std::unordered_map<std::string, std::string> content_type;
};