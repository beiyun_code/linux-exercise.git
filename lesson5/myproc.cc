#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <vector>



int main()
{
    printf("当前进程的开始代码!\n");
    execl("/usr/bin/ls", "ls", "--color=auto", "-l", NULL);
    exit(1);
    printf("当前进程的结束代码!\n");
    return 0;
}






//////////////////////////////////////////////////////////////////////// 
//using namespace std;
//typedef void (*hander_t)(); //函数指针类型
//
//
//vector<hander_t> handlers;
//void func1()
//{
//    for(int i = 1; i < 10; i++)
//    {
//        for(int j = 1; j <= i; j++ )
//        {
//            printf("%d * %d = %-2d ", i ,j , i*j );
//        }
//        printf("\n");
//    }
//
//}
//void func2()
//{
//    int ret = 0;
//    for(int i = 1; i <= 100; i++)
//    {
//        ret += i;
//    }
//    printf("%d\n",ret);
//}
//void Load()
//{
//    handlers.push_back(func1);
//    handlers.push_back(func2);
//}
// int main()
//{
//    pid_t id = fork();
//    if(id == 0)
//    {
//        int cnt = 5;
//        while(cnt--)
//        {
//            printf("我是子进程: %d\n",cnt);
//            sleep(1);
//        }
//        exit(1);
//    }
//    else 
//    {
//        //父进程
//        int quit = 0;
//        while(!quit)
//        {
//            int status = 0;
//	    	pid_t result = waitpid(-1, &status, WNOHANG);
//            if(result > 0)
//            {
//            //等待成功&&子进程并未退出
//            printf("等待子进程退出成功，退出码： %d\n",WEXITSTATUS(status));
//            }
//            else if (result == 0)
//            {
//                //等待成功 && 子进程并未退出
//                printf("子进程还在运行中，暂时还没有退出，父进程可以等一等，干干其他事情？\n");
//                if(handlers.empty()) Load();
//                for(auto it :handlers)
//                {
//                    it();
//                }
//            }
//            else 
//            {
//                //等待失败
//                printf("wait等待失败\n");
//                quit = 1;
//            }
//            sleep(1);
//            }   
//        }
//    
//}

//if(WIFEXITED(status))
//{
//    //子进程是正常退出的
//    printf("子进程执行完毕,子进程的退出码: %d\n", WEXITSTATUS(status));
//}
//else 
//{
//printf("子进程异常退出: %d\n", WIFEXITED(status));
//}
////////////////////////////////////////////////////////////////////////////// 
//pid_t result = waitpid(id,&status,0);//阻塞式等待等待子进程状态变化
//if(result > 0)
//{
//
//  printf("父进程等待成功, 退出码: %d, 退出信号： %d\n", (status>>8)&0xFF, status & 0x7F);
//}
    
