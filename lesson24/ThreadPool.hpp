#pragma once
#include <iostream>
#include <vector>
#include <queue>
#include <string>
#include <unistd.h>
#include <pthread.h>
class ThreadData
{
public:
    pthread_t tid;          // 线程ID
    std::string threadname; // 线程名字
};
const static int defaultnum = 5; // 默认5个线程
template <class T>
class ThreadPool
{
public:
    // 加锁
    void Lock()
    {
        pthread_mutex_lock(&_mutex);
    }
    // 解锁
    void UnLock()
    {
        pthread_mutex_unlock(&_mutex);
    }
    // 条件满足唤醒线程
    void WakeUp()
    {
        pthread_cond_signal(&_cond);
    }
    // 条件不满足线程休眠
    void ThreadSleep()
    {
        pthread_cond_wait(&_cond, &_mutex);
    }
    // 判断任务队列是否为空
    bool IsQueueEmpty()
    {
        return _tasks.empty();
    }
    std::string GetThreadName(pthread_t tid)
    {
        for (const auto &ti : _threads)
        {
            if (ti.tid == tid)
            {
                return ti.threadname;
            }
        }
        return "None";
    }

public:
    static void *HandlerTask(void *args)
    {
        ThreadPool<T> *tp = static_cast<ThreadPool<T> *>(args);
        std::string name = tp->GetThreadName(pthread_self());
        while (true)
        {
            tp->Lock();
            while (tp->IsQueueEmpty())
            {
                tp->ThreadSleep();
            }
            T t = tp->pop();
            tp->UnLock();
            t();
            // tp->Lock();
            // //std::cout << name << "正在处理任务..." << "运行结果是：" << t.GetResult() << std::endl;
            // tp->UnLock();
        }
    }
    void start() // 创建线程
    {
        int num = _threads.size();
        for (int i = 0; i < num; i++)
        {
            _threads[i].threadname = "thread-" + std::to_string(i + 1);
            pthread_create(&(_threads[i].tid), nullptr, HandlerTask, this);
        }
    }
    static ThreadPool<T> *GetInstance()
    {
        if (_tp == nullptr)
        {
            pthread_mutex_lock(&_lock);
            if (_tp == nullptr)
            {
                _tp = new ThreadPool<T>;
            }
            pthread_mutex_unlock(&_lock);
        }
        return _tp;
    }
    T pop()
    {
        T out = _tasks.front();
        _tasks.pop();
        return out;
    }
    void push(const T &in)
    {
        Lock();
        _tasks.push(in);
        WakeUp();
        UnLock();
    }

private:
    ThreadPool(int num = defaultnum)
        : _threads(num)
    {
        pthread_mutex_init(&_mutex, nullptr);
        pthread_cond_init(&_cond, nullptr);
    }
    ~ThreadPool()
    {
        pthread_mutex_destroy(&_mutex);
        pthread_cond_destroy(&_cond);
    }
    ThreadPool(const ThreadPool<T> &) = delete;
    const ThreadPool<T> &operator=(const ThreadPool<T> &) = delete;

private:
    std::vector<ThreadData> _threads; // 线程池
    std::queue<T> _tasks;             // 任务队列
    pthread_mutex_t _mutex;           // 互斥
    pthread_cond_t _cond;             // 同步
    static ThreadPool<T> *_tp;        // 单例模式
    static pthread_mutex_t _lock;     // 保证_tp线程安全
};
template <class T>
ThreadPool<T> *ThreadPool<T>::_tp = nullptr;
template <class T>
pthread_mutex_t ThreadPool<T>::_lock = PTHREAD_MUTEX_INITIALIZER;
