#pragma once
#include <iostream>
#include <string>
#include "log.hpp"
#include "Init.hpp"
extern Log lg;
Init init;
class Task
{
public:
    Task(int fd, std::string ip, uint16_t port)
        : sockfd(fd), clientip(ip), clientport(port)
    {
    }
    Task() {}
    void run()
    {
        char buffer[4096];
        while (true)
        {
            // TCP是字节流读取的。所以我们可以直接用系统调用接口read和write来进行读写
            ssize_t n = read(sockfd, buffer, sizeof(buffer));
            if (n < 0)
            {
                lg(Fatal, "read error,sockfd: %d,clientip: %s,clientport:%d", sockfd, clientip.c_str(), clientport);
                break;
            }
            else if (n == 0)
            {
                lg(Info, "%s:%d quit, server close sockfd: %d", clientip.c_str(), clientport, sockfd);
                break;
            }
            else
            {
                buffer[n] = 0;
                std::cout << "client say# " << buffer << std::endl;
                std::string echo_string = "tcpserver echo# ";
                echo_string += init.translation(buffer);
                write(sockfd, echo_string.c_str(), echo_string.size());
            }
        }
        close(sockfd);
    }
    void operator()()
    {
        run();
    }
    ~Task()
    {
    }

private:
    int sockfd;
    std::string clientip;
    uint16_t clientport;
};