
#pragma once
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "ThreadPool.hpp"
#include "Task.hpp"
#include "Daemon.hpp"
#include "log.hpp"
Log lg;
const int defaultsockfd = -1;
const uint16_t defaultport = 8080;
const std::string defaultip = "0.0.0.0";
const int backlog = 5;
enum
{
    SocketError = 1,
    BindError,
    ListenError
};

// class TcpServer;
// class ThreadData
// {
// public:
//     ThreadData(const int sockfd, const std::string ip, const uint16_t port, TcpServer *t)
//         : _sockfd(sockfd), _ip(ip), _port(port), tsrvr(t)
//     {
//     }

// public:
//     int _sockfd;
//     std::string _ip;
//     uint16_t _port;
//     TcpServer *tsrvr;
// };
class TcpServer
{

public:
    TcpServer(uint16_t serverport = defaultport, std::string serverip = defaultip)
        : _listensockfd(defaultsockfd), _serverport(serverport), _serverip(serverip)
    {
    }
    // static void *Routine(void *args)
    // {
    //     pthread_detach(pthread_self());
    //     ThreadData *td = static_cast<ThreadData *>(args);
    //     td->tsrvr->server(td->_sockfd, td->_ip, td->_port);
    //     delete td;
    //     return nullptr;
    // }
    void Init()
    {
        _listensockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (_listensockfd < 0)
        {
            lg(Fatal, "create listensockfd error: %d,errstring: %s", errno, strerror(errno));
            exit(SocketError);
        }
        lg(Info, "create listensockfd success, listensockfd: %d", _listensockfd);
        int opt = 1;
        setsockopt(_listensockfd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)); // 防止偶发性的服务器无法进行立即重启(tcp协议的时候再说)
        struct sockaddr_in local;
        memset(&local, 0, sizeof(local));
        local.sin_family = AF_INET;
        local.sin_port = htons(_serverport);
        inet_aton(_serverip.c_str(), &(local.sin_addr));
        socklen_t len = sizeof(local);
        if (bind(_listensockfd, (struct sockaddr *)(&local), len) < 0)
        {
            lg(Fatal, "bind listensockfd error: %d,errstring: %s", errno, strerror(errno));
            exit(BindError);
        }
        lg(Info, "bind listensockfd success, listensockfd: %d", _listensockfd);
        if (listen(_listensockfd, backlog) < 0)
        {
            lg(Fatal, "listen listensockfd error: %d,errstring: %s", errno, strerror(errno));
            exit(ListenError);
        }
        lg(Info, "listen listensockfd success, listensockfd: %d", _listensockfd);

        if (listen(_listensockfd, backlog) < 0)
        {
            lg(Fatal, "listen listensockfd error: %d,errstring: %s", errno, strerror(errno));
            exit(ListenError);
        }
        lg(Info, "listen listensockfd success, listensockfd: %d", _listensockfd);
    }
    void Start()
    {
        Daemon();
        ThreadPool<Task>::GetInstance()->start();
        for (;;)
        {
            struct sockaddr_in client;
            socklen_t len = sizeof(client);
            int sockfd = accept(_listensockfd, (struct sockaddr *)&client, &len);
            if (sockfd < 0)
            {
                lg(Warning, "accept listensockfd error: %d,errstring: %s", errno, strerror(errno));
                continue;
            }
            // 知道是谁请求连接，我们以IP地址和端口号来鉴别。
            uint16_t clientport = ntohs(client.sin_port);
            char clientip[16];
            // 这里我们用这个接口函数来把网络字节序转化为主机序列点分十的字符串
            inet_ntop(AF_INET, (struct sockaddr *)&client, clientip, sizeof(clientip));
            lg(Info, "get a new link..., sockfd: %d, client ip: %s, client port: %d", sockfd, clientip, clientport);

            // 写一个模拟服务器对数据做加工的服务函数
            // server(sockfd, clientip, clientport);

            // 多进程版本
            // pid_t id = fork();
            // // child
            // if (id == 0)
            // {
            //     close(_listensockfd);
            //     if (fork() > 0) // 孙子进程
            //         exit(0);
            //     server(sockfd, clientip, clientport);
            //     close(sockfd);
            //     exit(0);
            // }
            // // father
            // close(sockfd);
            // // 子进程我们不关心默认设为0
            // pid_t rid = waitpid(id, nullptr, 0);
            // (void)rid;

            // 多线程版本
            // ThreadData *td = new ThreadData(sockfd, clientip, clientport, this);
            // pthread_t tid;
            // pthread_create(&tid, nullptr, Routine, td);

            // 线程池版本
            Task t(sockfd, clientip, clientport);
            ThreadPool<Task>::GetInstance()->push(t);
        }
    }
    // void server(int sockfd, const std::string &clientip, const uint16_t &clientport)
    // {
    //     char buffer[4096];
    //     while (true)
    //     {
    //         // TCP是字节流读取的。所以我们可以直接用系统调用接口read和write来进行读写
    //         ssize_t n = read(sockfd, buffer, sizeof(buffer));
    //         if (n < 0)
    //         {
    //             lg(Fatal, "read error,sockfd: %d,clientip: %s,clientport:%d", sockfd, clientip.c_str(), clientport);
    //             break;
    //         }
    //         else if (n == 0)
    //         {
    //             lg(Info, "%s:%d quit, server close sockfd: %d", clientip.c_str(), clientport, sockfd);
    //             break;
    //         }
    //         else
    //         {
    //             buffer[n] = 0;
    //             std::cout << "client say#" << buffer << std::endl;
    //             std::string echo_string = "tcpserver echo# ";
    //             echo_string += buffer;
    //             write(sockfd, echo_string.c_str(), echo_string.size());
    //         }
    //     }
    // }

private:
    int _listensockfd;
    uint16_t _serverport;
    std::string _serverip;
};