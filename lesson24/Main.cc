#include "TcpServer.hpp"
#include <string>
#include <iostream>
#include <memory>

void Usage(const std::string &porc)
{
    std::cout<<"\n\rUsage:"<<porc <<"port[1024+]\n"<<std::endl;
}
int main(int argc, char* argv[])
{
    if(argc!=2)
    {
        Usage(argv[0]);
        exit(1);
    }
    uint16_t port = std::stoi(argv[1]); 
    lg.Enable(Classfile);
    std::unique_ptr<TcpServer> svr(new TcpServer(port));
    svr->Init();
    svr->Start();
    return 0;
}