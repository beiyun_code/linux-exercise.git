#include <iostream>
#include <string>
#include <pthread.h>
#include <unistd.h>

using namespace std;
class Response
{
public:
    Response(int exitcode, int result)
        : _exitcode(exitcode), _result(result) {}

public:
    int _exitcode; // 计算结果是否可靠
    int _result;   // 计算结果
};
class Request
{
public:
    Request(int start, int end, const string &threadname)
        : _start(start), _end(end), _threadname(threadname) {}

    static void *SumCount(void *args)
    {
        Request *rq = static_cast<Request *>(args);
        Response *rsp = new Response(0, 0);
        for (int i = rq->_start; i <= rq->_end; i++)
        {
            rsp->_result += i;
        }
        return (void *)rsp;
    }

public:
    int _start;
    int _end;
    string _threadname;
};
string toHex(pthread_t tid)
{
    char hex[64];
    snprintf(hex, sizeof(hex), "%p", tid);
    return hex;
}
int main()
{
    pthread_t tid;
    Request *rq = new Request(1, 100, "线程 1"); // 修正构造函数参数
    void *ret = nullptr;

    // 创建线程
    if (pthread_create(&tid, nullptr, Request::SumCount, rq) != 0)
    {
        cerr << "Error creating thread" << endl;
        delete rq; // 如果线程创建失败，释放 rq
        return 1;
    }

    // 等待线程结束
    if (pthread_join(tid, &ret) != 0)
    {
        cerr << "Error joining thread" << endl;
    }
    else
    {
        Response *rsp = static_cast<Response *>(ret);
        cout << rq->_threadname << " " << toHex(pthread_self()) << endl;
        cout << "Result: " << rsp->_result << " " << "Exitcode: " << rsp->_exitcode << endl;
        delete rsp; // 释放 Response 对象
    }

    delete rq; // 释放 Request 对象
    return 0;
}
// int g_val = 0;
// void show(const string &name)
// {
//     cout << name << "Say# " << "hello thread" << endl;
// }
// void *threadRoutine(void *args)
// {
//     int cnt = 5;
//     while (true)
//     {
//         // cout << "new thread , pid: " << getpid() << endl;
//         // show("[新线程]");
//         printf("new thread  g_val:%d  &_val:%p  pid:%d\n", g_val, &g_val, getpid());
//         cnt--;
//         sleep(1);

//         if(cnt == 0) break;
//     }
//     pthread_exit((void*)100);
//     //exit(1);
//     //return (void*)1;
// }

// int main()
// {
//     pthread_t tid;
//     pthread_create(&tid, nullptr, threadRoutine, nullptr);
//     // while (true)
//     // {
//     //     // cout << "main thread , pid: " << getpid() << endl;
//     //     // show("[主线程]");
//     //     printf("main thread  g_val:%d  &_val:%p  pid:%d\n", g_val, &g_val, getpid());
//     //     g_val++;
//     //     sleep(1);
//     // }
//     void* retval;
//     pthread_join(tid, &retval);
//     cout << "main thread quit..." << " " << " retval: "<< (long long int)retval << endl;
//     return 0;
// }
