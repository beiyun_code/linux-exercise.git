#include <iostream>
#include <cstdlib>
#include <string>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
using namespace std;
void myhandler(int signo)
{
    cout << "process get a signal: " << signo << endl;
    int n = alarm(10);
    cout << "上一个闹钟剩余时间: " << n << endl;
    // exit(1);
}


int main()
{
    signal(SIGALRM, myhandler);
    alarm(10);   //设定一个十秒后的闹钟

    while(true)
    {
         cout << "I am a crazy process " << getpid() << endl;
         sleep(1);
    };

    return 0;
}

// int main()
// {
//     int* ptr = nullptr;
//     *ptr = 10;

//     return 0;
// }

// int main()
// {
//     signal(SIGFPE,myhandler);
//     int a = 10;
//     a /= 0;
//     return 0;
// }
// void Usage(string proc)
// {
//     cout << "Usage:\n\t" << proc << " signum pid\n\n";
// }


// int main(int argc, char *argv[])
// {
//     //signal(2, myhandler);
//     signal(SIGABRT, myhandler);
//     int cnt = 0;
//     while (true)
//     {
//         cout << "I am a process, pid: " << getpid() << endl;
//         sleep(1);
//         cnt++;
//         if (cnt % 2 == 0)
//         {
//             // kill(getpid(), 2);
//             //raise(2);
//             abort();
//             //  kill(getpid(), 6);
//         }
//     }

// if(argc != 3) //提示用户怎么用kill命令
// {
//     Usage(argv[0]);
//     exit(1);
// }
// int signum = stoi(argv[1]); //stoi将字符转换成数字
// pid_t pid = stoi(argv[2]);

// int n = kill(pid, signum);
// if(n == -1) //差错处理
// {
//     perror("kill");
//     exit(2);
// }

//     return 0;
// }
// void myhandler(int signo)
// {
//     cout << "process get a signal: " << signo << endl;
// }

// int main()
// {
//     for(int i = 1; i<=31 ; i++)
//     {
//         signal(i,myhandler);
//     }
//     while(true)
//     {
//         cout << "I am process.... pid: "  << getpid() << endl;
//         sleep(1);
//     }
//     return 0;
// }