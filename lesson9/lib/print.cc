#include "print.h"
#include <iostream>
#include <ctime>

void printTime() {
    // 获取当前时间
    time_t now = time(nullptr);
    
    // 使用ctime转换时间
    char* c_time_string = ctime(&now);
    
    // 输出转换后的时间
    std::cout << "当前时间（ctime）: " << c_time_string << std::endl;
}


