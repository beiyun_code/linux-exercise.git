#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <string.h>


int main()
{
    //c语言提供的
    const char *s = "hello world1\n";
    const char *s1 = "hello world2\n";
    printf("hello linux\n");
    fprintf(stdout, "%s", s);
    fputs(s1, stdout);
    //os提供的
    const char *s2 = "hello world3\n";
    write(1, s2, strlen(s2));

    //创建子进程
    fork();
    return 0;
}