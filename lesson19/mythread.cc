#include <pthread.h>
#include <unistd.h>
#include <vector>
#include <string>
#include <cstring>
#include <cstdio>
#include <iostream>
using namespace std;
class ThreadData
{
public:
    ThreadData(int number)
    {
        _threadname = "thread-" + to_string(number);
    }
    string GetName()
    {
        return _threadname;
    }

private:
    string _threadname;
};
#define NUM 5
int tickets = 1000;                               // 火车票
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER; // 静态分配。
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;   // 条件变量静态全局变量
void *GetTickets(void *args)
{
    ThreadData *td = static_cast<ThreadData *>(args);
    const char *name = td->GetName().c_str();
    while (true)
    {
        pthread_mutex_lock(&lock);
        pthread_cond_wait(&cond, &lock);
        if (tickets > 0)
        {
            usleep(1000);
            tickets--;
            printf("线程:%s抢到一张票,tickets剩余:%d\n", name, tickets);
            pthread_mutex_unlock(&lock);
        }

        else
        {
            pthread_mutex_unlock(&lock);
            break;
        }
    }

    printf("线程:%s ... 退出\n", name);
    return nullptr;
}
int main()
{
    // 创建多线程
    vector<pthread_t> tids;            // 数组放线程ID
    vector<ThreadData *> thread_datas; // 线程信息
    for (int i = 1; i <= NUM; i++)
    {
        pthread_t tid;
        ThreadData *td = new ThreadData(i);
        thread_datas.emplace_back(td);
        pthread_create(&tid, nullptr, GetTickets, thread_datas[i - 1]);
        tids.emplace_back(tid);
    }
    while (tickets > 0)
    {
        usleep(1000);
        pthread_cond_signal(&cond);
        cout << " 主线程唤醒新线程..." << endl;
    }
    pthread_cond_broadcast(&cond); //这里需要唤醒所有等待的线程
    for (auto thread : tids)
    {
        pthread_join(thread, nullptr);
    }
    for (auto td : thread_datas)
    {
        delete td;
    }

    return 0;
}