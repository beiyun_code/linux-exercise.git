#pragma once
#include <iostream>
#include <cerrno>
#include <cstring>
#include <cstdlib>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string>

#define FIFO_FILE "./myFileName"
#define MODE 0664

// 退出码
enum
{
    FIFO_CREATE_ERR = 1, // 管道创建失败退出码设置为1
    FIFO_DELETE_ERR,
    FIFO_OPEN_ERR,
};
class Init
{
public:
    Init()
    {
        int n = mkfifo(FIFO_FILE, MODE);
        if (n == -1)
        {
            perror("mkfifo");
            exit(FIFO_CREATE_ERR);
        }
    }
    ~Init()
    {
        int m = unlink(FIFO_FILE);
        if(m == -1)
        {
            perror("unlink");
            exit(FIFO_DELETE_ERR);
        }
    }
};