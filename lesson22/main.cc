// #include "ThreadPool.hpp"
// #include "Task.hpp"
// #include <ctime>
// int main()
// {
//     srand(time(nullptr) ^ getpid());
//     ThreadPool<Task>::GetInstance()->start();
//     while (true)
//     {

//         int x = rand() % 10 + 1;
//         int y = rand() % 10;
//         char oper = opers[rand() % opers.size()];
//         Task t(x, y, oper);
//         ThreadPool<Task>::GetInstance()->push(t);
//         ThreadPool<Task>::GetInstance()->Lock();
//         std::cout << "主线程发布任务..." << t.GetTask() << std::endl;
//         ThreadPool<Task>::GetInstance()->UnLock();
//         sleep(1);
//     }
//     return 0;
// }
#include <iostream>
#include <pthread.h>
#include <unistd.h>

const int MAX_READERS = 5;
const int NUM_READERS = 3;
const int NUM_WRITERS = 2;

// 共享资源
int shared_data = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t no_readers = PTHREAD_COND_INITIALIZER;
pthread_cond_t no_writers = PTHREAD_COND_INITIALIZER;

// 线程参数结构体
struct ThreadParam {
    int id;
};

// 读者线程函数
void* reader(void* arg) {
    ThreadParam* param = static_cast<ThreadParam*>(arg);
    int reader_id = param->id;

    while (true) {
        pthread_mutex_lock(&mutex);
        while (shared_data < 0) {
            pthread_cond_wait(&no_writers, &mutex);
        }
        pthread_mutex_unlock(&mutex);

        std::cout << "Reader " << reader_id << " reads: " << shared_data << std::endl;
        sleep(1); // 模拟读取时间

        // 读者读取完毕，唤醒写者
        pthread_mutex_lock(&mutex);
        pthread_cond_signal(&no_writers);
        pthread_mutex_unlock(&mutex);
    }

    return nullptr;
}

// 写者线程函数
void* writer(void* arg) {
    ThreadParam* param = static_cast<ThreadParam*>(arg);
    int writer_id = param->id;

    while (true) {
        pthread_mutex_lock(&mutex);
        while (shared_data >= 0) {
            pthread_cond_wait(&no_readers, &mutex);
        }
        shared_data--; // 写者进入
        pthread_mutex_unlock(&mutex);

        std::cout << "Writer " << writer_id << " writes: " << -shared_data << std::endl;
        sleep(2); // 模拟写入时间

        pthread_mutex_lock(&mutex);
        shared_data++; // 写者退出
        pthread_cond_broadcast(&no_writers); // 唤醒所有读者
        pthread_mutex_unlock(&mutex);
    }

    return nullptr;
}

int main() {
    pthread_t readers[NUM_READERS], writers[NUM_WRITERS];
    ThreadParam params_readers[NUM_READERS], params_writers[NUM_WRITERS];

    // 创建读者线程
    for (int i = 0; i < NUM_READERS; ++i) {
        params_readers[i].id = i;
        pthread_create(&readers[i], nullptr, reader, &params_readers[i]);
    }

    // 创建写者线程
    for (int i = 0; i < NUM_WRITERS; ++i) {
        params_writers[i].id = i + NUM_READERS;
        pthread_create(&writers[i], nullptr, writer, &params_writers[i]);
    }

    // 等待线程结束
    for (int i = 0; i < NUM_READERS + NUM_WRITERS; ++i) {
        pthread_join(readers[i], nullptr);
    }

    return 0;
}