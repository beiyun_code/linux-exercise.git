#include <iostream>
#include <signal.h>
#include <unistd.h>

using namespace std;

void myhandler(int singo)
{
    cout << "catch a singo: " << singo << endl;
}
void print_pending(sigset_t &pending)
{
    for (int signo = 31; signo >= 1; signo--)
    {
        if (sigismember(&pending, signo))
        {
            cout << "1";
        }
        else
        {
            cout << "0";
        }
    }
    cout << "\n\n";
}
int main()
{
    // 1.对2号信号进行自定义捕捉
    signal(2, myhandler);
    // 2.1先对2号信号进行屏蔽 --- 数据准备
    sigset_t bset, oset;
    sigemptyset(&bset);
    sigemptyset(&oset);
    sigaddset(&bset, 2);
    // 2.2调用系统调用，将数据设置进内核
    sigprocmask(SIG_SETMASK, &bset, &oset);
    // 3.重复打印当前进程的pending表  0000 0000 0000 0000 0000 0000 0000 0000
    sigset_t pending;
    int cnt = 0;
    while (true)
    {
        // 3.1 获取
        int n = sigpending(&pending);
        if (n < 0)
            continue;
        // 3.2 打印
        print_pending(pending);
        sleep(1);
        // 3.3解除阻塞
        cnt++;
        if(cnt == 20)
        {
            cout << "unblock 2 signo" << endl;
            sigprocmask(SIG_SETMASK, &oset, nullptr);
        }
    }
    // 4.发送2号信号0000 0000 0000 0000 0000 0000 0000 0010信号

    return 0;
}