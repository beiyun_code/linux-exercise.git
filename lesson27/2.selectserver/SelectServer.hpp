#pragma once
#include <iostream>
#include <sys/time.h>
#include <sys/select.h>
#include "Socket.hpp"
static const int fd_num_max = (sizeof(fd_set) * 8);
static const uint16_t defaultport = 8080;
const int defaultfd = -1;
class SelectServer
{
public:
    SelectServer(uint16_t port = defaultport)
        : _port(port)
    {
        for (int i = 0; i < fd_num_max; i++)
        {
            fd_array[i] = defaultfd;
        }
    }
    bool Init()
    {
        _listensock.Socket();
        _listensock.Bind(_port);
        _listensock.Listen();

        return true;
    }
    void Accepter()
    {
        std::string clientip;
        uint16_t clientport = 0;
        int sock = _listensock.Accept(&clientip, &clientport);
        if (sock < 0)
            return;
        lg(Info, "accept success, %s: %d, sock fd: %d", clientip.c_str(), clientport, sock);
        int pos = 1;
        for (; pos < fd_num_max; pos++)
        {
            if (fd_array[pos] != defaultfd)
                continue;
            else
                break;
        }
        if (pos == fd_num_max)
        {
            lg(Warning, "server is full close %d now! ", sock);
            close(sock);
        }
        else
        {
            fd_array[pos] = sock;
            PrintFd();
        }
    }
    void Read(int fd, int pos)
    {
        char buffer[1024];
        ssize_t n = read(fd, &buffer, sizeof(buffer) - 1);
        if (n > 0)
        {
            buffer[n] = 0;
            std::cout << "get a message: " << buffer << std::endl;
        }
        else if (n == 0)
        {
            // 说明客户关闭了连接
            lg(Info, "client qiut ,me too close fd is: %d", fd);
            close(fd);
            // 客户端都不玩了，所以我们需要将数组的fd移除，本质是从select中移除。
            fd_array[pos] = defaultfd;
        }
        else
        {
            // 这里说明读出错了，还玩什么？
            lg(Warning, "read error: fd is : %d", fd);
            close(fd);
            fd_array[pos] = defaultfd;
        }
    }
    void Dispatcher(fd_set &rfds)
    {
        for (int i = 0; i < fd_num_max; i++)
        {
            int fd = fd_array[i];
            if (fd == defaultfd)
                continue;
            if (FD_ISSET(fd, &rfds))
            {
                if (fd == _listensock.Fd())
                {
                    Accepter();
                }
                else
                {
                    Read(fd, i);
                }
            }
        }
    }
    void Start()
    {
        int listensock = _listensock.Fd();
        fd_array[0] = listensock;
        while (1)
        {
            fd_set rfds;
            FD_ZERO(&rfds);
            int maxfd = fd_array[0];
            for (int i = 0; i < fd_num_max; i++)
            {
                if (fd_array[i] == defaultfd)
                    continue;
                FD_SET(fd_array[i], &rfds);
                if (maxfd < fd_array[i])
                {
                    maxfd = fd_array[i];
                }
            }
            struct timeval timeout = {2, 0};
            int n = select(maxfd + 1, &rfds, nullptr, nullptr, &timeout);
            switch (n)
            {
            case 0:
                // 超时了
                std::cout << "time out timeout:" << timeout.tv_sec << "." << timeout.tv_usec << std::endl;
                break;
            case -1:
                // 出错了
                std::cerr << "select error" << std::endl;
                break;

            default:
                // fd就绪了,但是fd可不只有socket返回的还有accpet返回的，所以这里我们区分fd类型，然后做派发
                Dispatcher(rfds);
                break;
            }
        }
    }
    void PrintFd()
    {
        std::cout << "online fd list: ";
        for (int i = 0; i < fd_num_max; i++)
        {
            if (fd_array[i] == defaultfd)
                continue;
            std::cout << fd_array[i] << " ";
        }
        std::cout << std::endl;
    }
    ~SelectServer()
    {
        _listensock.Close();
    }

private:
    Sock _listensock;
    uint16_t _port;
    int fd_array[fd_num_max];
};