#pragma once
#include <iostream>
#include <memory>
#include "Epoller.hpp"
#include "Socket.hpp"
#include "nocopy.hpp"
uint32_t EVENT_IN = (EPOLLIN);
uint32_t EVENT_OUT = (EPOLLOUT);
static const uint16_t defaultport = 8080;
class EpollServer : public nocopy
{
    static const int num = 64;

public:
    EpollServer(uint16_t port = defaultport)
        : _port(port), _ptr_listensock(new Sock()), _ptr_epoller(new Epoller())
    {
    }
    void Init()
    {
        _ptr_listensock->Socket();
        _ptr_listensock->Bind(_port);
        _ptr_listensock->Listen();
        lg(Info, "create listen socket success: %d", _ptr_listensock->Fd());
    }
    void Accepter()
    {
        std::string clientip;
        uint16_t clientport;
        int sock = _ptr_listensock->Accept(&clientip, &clientport);
        if (sock > 0)
        {
            _ptr_epoller->EpollerUpdate(EPOLL_CTL_ADD, sock, EVENT_IN);
            lg(Info, "get a new link, client info@ %s:%d", clientip.c_str(), clientport);
        }
    }
    void Recver(int fd)
    {
        char buffer[1024];
        ssize_t n = read(fd,buffer,sizeof(buffer)-1);
        if(n>0)
        {
            buffer[n] = 0;
            std::cout << "get a message: "<< buffer <<std::endl;
            std:: string echo_str = "server echo $";
            echo_str += buffer; 
            write(fd,echo_str.c_str(),echo_str.size());
        }
    }
    void Dispatcher(struct epoll_event revs[], int num)
    {
        for (int i = 0; i < num; i++)
        {
            uint32_t events = revs[i].events;
            int fd = revs[i].data.fd;
            if (events & EVENT_IN)
            {
                if (fd == _ptr_listensock->Fd())
                {
                    // 接受
                    Accepter();
                }
                else
                {
                    Recver(fd);
                }
            }
            else if (events & EVENT_OUT)
            {
            }
            else
            {
            }
        }
    }
    void Start()
    {
        // 将我们的_listensock和它要关心的事件 添加到epoll模型中。
        _ptr_epoller->EpollerUpdate(EPOLL_CTL_ADD, _ptr_listensock->Fd(), EVENT_IN);
        struct epoll_event revs[num];
        for (;;)
        {
            int n = _ptr_epoller->EpollerWait(revs, num);
            if (n > 0)
            {
                // 有事件就绪
                lg(Debug, "event happened, fd is : %d", revs[0].data.fd);
                // 派发 根据fd类型
                Dispatcher(revs, num);
            }
            else if (n == 0)
            {
                lg(Info, " time out");
            }
            else
            {
                lg(Error, "epoll_wait error");
            }
        }
    }
    ~EpollServer()
    {
        _ptr_listensock->Close();
    }

private:
    std::shared_ptr<Sock> _ptr_listensock;
    std::shared_ptr<Epoller> _ptr_epoller;
    uint16_t _port;
};