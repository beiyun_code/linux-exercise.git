#pragma once
#include "log.hpp"
#include "nocopy.hpp"
#include <cerrno>
#include <cstring>
#include <sys/epoll.h>
extern Log lg;
static const int size = 128;
class Epoller:public nocopy
{

public:
    Epoller()
    {
        _epfd = epoll_create(size);
        if (_epfd == -1)
        {
            lg(Error, "epoll_crate error: %s", strerror(errno));
        }
        else
        {
            lg(Info, " epoll_crate success: %d", _epfd);
        }
    }
    int EpollerWait(struct epoll_event revents[], int num)
    {
        int n = epoll_wait(_epfd, revents, num, -1);
        return n;
    }
    int EpollerUpdate(int oper, int sock, uint32_t event)
    {
        int n = 0;
        if (oper == EPOLL_CTL_DEL)
        {
            n = epoll_ctl(_epfd, oper, sock, nullptr);
            if (n != 0)
            {
                lg(Error, "epoll_ctl error: %s", strerror(errno));
            }
        }
        else
        {
            struct epoll_event ev;
            ev.events = event;
            ev.data.fd = sock; // 方便我们知道那一个fd就绪了

            n = epoll_ctl(_epfd, oper, sock, &ev);
            
            if (n != 0)
            {
                lg(Error, "epoll_ctl error");
            }
        }
        return n;
    }
    ~Epoller()
    {
        if (_epfd >= 0)
        {
            close(_epfd);
        }
    }

private:
    int _epfd;
    int timeout{2000};
};