#include <iostream>
#include "EpollerSever.hpp"
#include <memory>

int main()
{
    std::unique_ptr<EpollServer> epoll_ptr(new EpollServer(8080));
    epoll_ptr->Init();
    epoll_ptr->Start();
    return 0;
}