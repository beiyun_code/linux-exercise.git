#pragma once
#include <iostream>
#include <sys/time.h>
#include <poll.h>
#include "Socket.hpp"
static const int fd_num_max = 64;
static const uint16_t defaultport = 8080;
const int defaultfd = -1;
const int non_event = 0;
class PollServer
{
public:
    PollServer(uint16_t port = defaultport)
        : _port(port)
    {
        for (int i = 0; i < fd_num_max; i++)
        {
             _event_fds[i].fd = defaultfd;
             _event_fds[i].events = non_event;
             _event_fds[i].revents = non_event;
        }
    }
    bool Init()
    {
        _listensock.Socket();
        _listensock.Bind(_port);
        _listensock.Listen();

        return true;
    }
    void Accepter()
    {
        std::string clientip;
        uint16_t clientport = 0;
        int sock = _listensock.Accept(&clientip, &clientport);
        if (sock < 0)
            return;
        lg(Info, "accept success, %s: %d, sock fd: %d", clientip.c_str(), clientport, sock);
        int pos = 1;
        for (; pos < fd_num_max; pos++)
        {
            if (_event_fds[pos].fd != defaultfd)
                continue;
            else
                break;
        }
        if (pos == fd_num_max)
        {
            lg(Warning, "server is full close %d now! ", sock);
            close(sock);
        }
        else
        {
            _event_fds[pos].fd = sock;
            _event_fds[pos].events = POLLIN;
            _event_fds[pos].revents = non_event;
            PrintFd();
        }
    }
    void Read(int fd, int pos)
    {
        char buffer[1024];
        ssize_t n = read(fd, &buffer, sizeof(buffer) - 1);
        if (n > 0)
        {
            buffer[n] = 0;
            std::cout << "get a message: " << buffer << std::endl;
        }
        else if (n == 0)
        {
            // 说明客户关闭了连接
            lg(Info, "client qiut ,me too close fd is: %d", fd);
            close(fd);
            // 客户端都不玩了，所以我们需要将数组的fd移除，本质是从select中移除。
            _event_fds[pos].fd = defaultfd;
        }
        else
        {
            // 这里说明读出错了，还玩什么？
            lg(Warning, "read error: fd is : %d", fd);
            close(fd);
            _event_fds[pos].fd = defaultfd;
        }
    }
    void Dispatcher()
    {
        for (int i = 0; i < fd_num_max; i++)
        {
            int fd = _event_fds[i].fd;
            if (fd == defaultfd)
                continue;
            if (_event_fds[i].revents & POLLIN)
            {
                if (fd == _listensock.Fd())
                {
                    Accepter();
                }
                else
                {
                    Read(fd, i);
                }
            }
        }
    }
    void Start()
    {
        int listensock = _listensock.Fd();
        _event_fds[0].fd = listensock;
        _event_fds[0].events = POLLIN;
        int timeout = 2000; //2s
        while (1)
        {
            int n  = poll(_event_fds,fd_num_max,timeout);
            
            switch (n)
            {
            case 0:
                // 超时了
                std::cout << "time out " << std::endl;
                break;
            case -1:
                // 出错了
                std::cerr << "poll error" << std::endl;
                break;

            default:
                // fd就绪了,但是fd可不只有socket返回的还有accpet返回的，所以这里我们区分fd类型，然后做派发
                Dispatcher();
                break;
            }
        }
    }
    void PrintFd()
    {
        std::cout << "online fd list: ";
        for (int i = 0; i < fd_num_max; i++)
        {
            if (_event_fds[i].fd == defaultfd)
                continue;
            std::cout << _event_fds[i].fd << " ";
        }
        std::cout << std::endl;
    }
    ~PollServer()
    {
        _listensock.Close();
    }

private:
    Sock _listensock;
    uint16_t _port;
    struct pollfd _event_fds[fd_num_max];
};