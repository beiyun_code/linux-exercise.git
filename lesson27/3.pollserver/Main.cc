#include <iostream>
#include <memory>
#include "PollServer.hpp"

int main()
{
    std::unique_ptr<PollServer> svr(new PollServer());
    svr->Init();
    svr->Start();
    return 0;
}