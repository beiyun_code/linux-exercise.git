#include <pthread.h>
#include <unistd.h>
#include <vector>
#include <string>
#include <cstring>
#include <cstdio>
#include "lockGuard.hpp"
#include <iostream>
using namespace std;
class ThreadData
{
public:
    ThreadData(int number)
    {
        _threadname = "thread-" + to_string(number);
    }
    string GetName()
    {
        return _threadname;
    }

private:
    string _threadname;
};
#define NUM 5
int tickets = 1000;   // 火车票
pthread_mutex_t lock; // 动态要初始化
// pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER; //静态分配。
void *GetTickets(void *args)
{
    ThreadData *td = static_cast<ThreadData *>(args);
    const char *name = td->GetName().c_str();
    pthread_mutex_lock(&lock);
    while (true)
    {
        //pthread_mutex_lock(&lock);
        // lockGudard lockguard(&lock); //ARII
        if (tickets >= 0)
        {
            usleep(1000);
            printf("线程:%s抢到一张票,tickets剩余:%d\n", name, tickets);
            tickets--;
            //pthread_mutex_unlock(&lock);
        }

        else
        {
            //pthread_mutex_unlock(&lock);
            break;
        }
    }
    pthread_mutex_unlock(&lock);
    printf("线程:%s ... 退出\n", name);
  

    return nullptr;
}
int main()
{
    pthread_mutex_init(&lock, nullptr);
    // 创建多线程
    vector<pthread_t> tids;            // 数组放线程ID
    vector<ThreadData *> thread_datas; // 线程信息
    for (int i = 1; i <= NUM; i++)
    {
        pthread_t tid;
        ThreadData *td = new ThreadData(i);
        thread_datas.emplace_back(td);
        pthread_create(&tid, nullptr, GetTickets, thread_datas[i - 1]);
        tids.emplace_back(tid);
    }
    for (auto thread : tids)
    {
        pthread_join(thread, nullptr);
    }
    for (auto td : thread_datas)
    {
        delete td;
    }
    pthread_mutex_destroy(&lock);
    return 0;
}