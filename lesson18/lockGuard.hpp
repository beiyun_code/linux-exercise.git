#pragma once
#include <pthread.h>
class Mutex
{
public:
    Mutex(pthread_mutex_t *lock)
        : _lock(lock)
    {
    }
    void lock()
    {
        pthread_mutex_lock(_lock);
    }
    void unlock()
    {
        pthread_mutex_unlock(_lock);
    }
    ~Mutex() {}

private:
    pthread_mutex_t *_lock;
};
class lockGudard
{
public:
    lockGudard(pthread_mutex_t *lock)
        : _mutex(lock)
    {
        _mutex.lock();
    }
    ~lockGudard()
    {
        _mutex.unlock();
    }
private:
    Mutex _mutex;
};