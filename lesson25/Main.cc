#include "TcpServer.hpp"
#include "ServerCal.hpp"
#include <memory>

int main()
{
    ServerCal cal;
    TcpServer *svr = new TcpServer(8080, std::bind(&ServerCal::Calculator, &cal, std::placeholders::_1));
    svr->Init();
    svr->Start();

    return 0;
}