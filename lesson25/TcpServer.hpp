#pragma once
#include <iostream>
#include <string>
#include <thread>
#include <functional>
#include "Socket.hpp"
using func_t = std::function<std::string(std::string &packge)>;
class TcpServer;
class ThreadData
{
public:
    ThreadData(int sock, std::string &ip, uint16_t port, TcpServer *tpsvr)
        : _sock(sock), _ip(ip), _port(port), _tpsvr(tpsvr)
    {
    }

public:
    int _sock;
    std::string _ip;
    uint16_t _port;
    TcpServer *_tpsvr; // 回调指针
};

class TcpServer
{

public:
    TcpServer(const uint16_t port = 8080, func_t callback = nullptr)
        : _port(port), _callback(callback) {}
    // 初始化
    void Init()
    {
        _listensock.Socket();
        _listensock.Bind(_port);
        _listensock.Listen();
    }
    static void ServerIO(ThreadData *td)
    {
        // TODO
        TcpServer *tpsvr = td->_tpsvr;
        int sock = td->_sock;
        std::string inbuffer_stream;
        while (true)
        {
            char buffer[1024];
            ssize_t n = read(sock, buffer, sizeof(buffer));
            if (n > 0)
            {
                buffer[n] = 0;
                inbuffer_stream += buffer;
                lg(Debug, "debug:\n%s", inbuffer_stream.c_str());
                while (true)
                {
                    std::string info = tpsvr->_callback(inbuffer_stream); // 业务处理
                    if (info.empty())
                    {
                        break;
                    }
                    lg(Debug, "respose:\n%s", info.c_str());
                    write(sock, info.c_str(), info.size());
                }
            }
            else if (n == 0)
            {
                // Connection closed by client
                break;
            }
            else
            {
                // Error
                lg(Error, "Read error");
                break;
            }
        }
    }
    // 启动服务器
    void Start()
    {
        for (;;)
        {
            std::string clientip;
            uint16_t clientport;
            int sockfd = _listensock.Accept(&clientip, &clientport);
            if (sockfd < 0)
            {
                continue;
            }
            lg(Info, "accept get a new link,sockfd: %d,clientip: %s,clientport: %d", sockfd, clientip.c_str(), clientport);
            // 创建线程处理任务
            ThreadData *td = new ThreadData(sockfd, clientip, clientport, this);
            std::thread t(ServerIO, td);
            // 线程分离
            t.detach();
        }
    }

    ~TcpServer()
    {
        _listensock.Close();
    }

private:
    Sock _listensock;
    uint16_t _port;
    func_t _callback;
};
