#include <iostream>
#include <jsoncpp/json/json.h>

int main()
{
    Json::Value part1;
    part1["haha"] = "haha";
    part1["hehe"] = "hehe";
    std::cout << part1 << std::endl;
    Json::Value root;
    root["x"] = 1;
    root["y"] = 1;
    root["op"] = '+';
    root["dest"] = "this is a + oper";
    root["test"] = part1;
    Json::StyledWriter w;
    std::string res = w.write(root);
    std::cout << res << std::endl;
    return 0;
}