#include "comm.hpp"

int main()
{
    int shmid = createShm();
    char *shmaddr = (char *)shmat(shmid, nullptr, 0);

    while (true)
    {

        cout << "Enter a message: " << shmaddr;
        fgets(shmaddr, 4096, stdin);
        
    }

    shmdt(shmaddr);
    shmctl(shmid, IPC_RMID, nullptr);

    return 0;
}