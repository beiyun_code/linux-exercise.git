#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>


using namespace std;
const int SHM_SIZE = 4096; // 共享内存的大小 也就是4KB，我们申请的大小一般是4096的整数倍
const string pathname = "/home/gx/linux-exercise";
const int proj_id = 0x666; // 这个你可以自己随便定一个，只要保证唯一就行

key_t get_key()
{
    key_t key = ftok(pathname.c_str(), proj_id);
    if (key == -1)
    {
        cerr << "ftok error" << endl;
        exit(1);
    }
    return key;
}

int create_shm(int flag)
{
    key_t key = get_key();
    int n = shmget(key, SHM_SIZE, flag);
    if (n < 0)
    {
        cerr << "shmget error" << endl;
        exit(1);
    }
    return n;
}

int createShm()
{
    return create_shm(IPC_CREAT | IPC_EXCL | 0666);
}
int getShm()
{
    return create_shm(IPC_CREAT);
}
